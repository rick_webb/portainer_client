
import json
import requests
import sys

class api():
    def __init__(self, portainer_url, verifySSL=True):
        self.portainer_server = "{url}/api".format(url=portainer_url)
        self.verifySSL = verifySSL

    def login(self, username, password):
        r = requests.post(
            self.portainer_server+"/auth", 
            data=json.dumps({"Username":username, "Password":password}), 
            verify=self.verifySSL)
        j = r.json()
        if 'err' in j:
            print("{detail} : {error}".format(detail=j['details'], error=j['err']))
            sys.exit(1)
        else: pass
        self.token = j.get("jwt")

    def get_endpoint(self, identifier):
        r = requests.get(
            self.portainer_server+"/endpoints/{id}".format(id=identifier), 
            headers={"Authorization": "Bearer {token}".format(token=self.token)}, 
            verify=self.verifySSL)
        return  r.json()

    def get_dockerhub_info(self):
        r = requests.get(
            self.portainer_server+"/dockerhub",
            headers={"Authorization": "Bearer {token}".format(token=self.token)},
            verify=self.verifySSL)
        return r.json()

    def put_dockerhub_info(self, options):
        r = requests.put(
            self.portainer_server+"/dockerhub",
            data=json.dumps(options),
            headers={"Authorization": "Bearer {token}".format(token=self.token)},
            verify=self.verifySSL)
        return r.json()

    def get_status(self):
        r = requests.get(
            self.portainer_server+"/status",
            headers={"Authorization": "Bearer {token}".format(token=self.token)},
            verify=self.verifySSL)
        return r.json()

    def get_endpoints(self):
        r = requests.get(
            self.portainer_server+"/endpoints",
            headers={"Authorization": "Bearer {token}".format(token=self.token)},
            verify=self.verifySSL)
        return r.json()

    def new_endpoint(self, options):
        r = requests.post(
            self.portainer_server+"/endpoints",
            data=json.dumps(options),
            headers={"Authorization": "Bearer {token}".format(token=self.token)},
            verify=self.verifySSL)
        return r.json()

    def get_endpoint(self, identifier):
        r = requests.get(
            self.portainer_server+"/endpoints/{id}".format(id=identifier),
            headers={"Authorization": "Bearer {token}".format(token=self.token)},
            verify=self.verifySSL)
        return r.json()

    def update_endpoint(self, identifier, options):
        r = requests.put(
            self.portainer_server+"/endpoints/{id}".format(id=identifier),
            data=json.dumps(options),
            headers={"Authorization": "Bearer {token}".format(token=self.token)},
            verify=self.verifySSL)
        return r.json()

    def delete_endpoint(self, identifier):
        r = requests.delete(
            self.portainer_server+"/endpoints/{id}".format(id=identifier),
            headers={"Authorization": "Bearer {token}".format(token=self.token)},
            verify=self.verifySSL)
        return r.json()

    def access_endpoint(self, identifier, options):
        r = requests.put(
            self.portainer_server+"/endpoints/{id}/access".format(id=identifier),
            data=json.dumps(options),
            headers={"Authorization": "Bearer {token}".format(token=self.token)},
            verify=self.verifySSL)
        return r.json()

    def get_stacks(self, endpoint):
        r = requests.get(
            self.portainer_server + "/endpoints/{endpoint}/stacks".format(endpoint=endpoint),
            headers={"Authorization": "Bearer {token}".format(token=self.token)},
            verify=self.verifySSL)
        return r.json()

    def new_stack(self, endpoint, options):
        r = requests.post(
            self.portainer_server + "/endpoints/{endpoint}/stacks".format(endpoint=endpoint),
            data=json.dumps(options),
            headers={"Authorization": "Bearer {token}".format(token=self.token)},
            verify=self.verifySSL)
        return r.json()

    def get_stack(self, endpoint, stack):
        r = requests.get(
            self.portainer_server + "/endpoints/{endpoint}/stacks/{stack}".format(endpoint=endpoint, stack=stack),
            headers={"Authorization": "Bearer {token}".format(token=self.token)},
            verify=self.verifySSL)
        return r.json()

    def update_stack(self, endpoint, stack, options):
        r = requests.put(
            self.portainer_server + "/endpoints/{endpoint}/stacks/{stack}".format(endpoint=endpoint, stack=stack),
            headers={"Authorization": "Bearer {token}".format(token=self.token)},
            verify=self.verifySSL)
        return r.json()

    def delete_stack(self, endpoint, stack):
        r = requests.delete(
            self.portainer_server + "/endpoints/{endpoint}/stacks/{stack}".format(endpoint=endpoint, stack=stack),
            headers={"Authorization": "Bearer {token}".format(token=self.token)},
            verify=self.verifySSL)
        return r.json()

    def get_stackfile(self, endpoint, stack):
        r = requests.get(
            self.portainer_server + "/endpoints/{endpoint}/stacks/{stack}/stackfile".format(endpoint=endpoint, stack=stack),
            headers={"Authorization": "Bearer {token}".format(token=self.token)},
            verify=self.verifySSL)
        return r.json()
