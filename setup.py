from setuptools import find_packages, setup 
with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="portainer_client",
    version="0.0.1",
    author="rick_webb",
    url="https://gitlab.com/rick_webb/portainer_client",
    description="portainer api client",
    long_description=long_description,
    long_description_content_type="text/markdown",
    install_requires=["requests >= 2.2.1"],
    packages=find_packages(),
    py_modules=["portainer_client"],
    classifiers=[
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
