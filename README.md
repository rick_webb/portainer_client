# About

# Example

Examples are contained within the examples directory
```
import portainer_client

user   = "<USERNAME>"
passwd = "<PASSWORD>"
server = "<PORTAINER_SERVER_INSTANCE>:9000"

connection = portainer.api(server)
connection.login(user, passwd)

```
# Documentation
[Portainer API](https://app.swaggerhub.com/apis/deviantony/Portainer/1.20.2/)

